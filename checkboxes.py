#===================================================================================
# This program demonstrates how you might read and write to and from checkboxes
# Warren Sutton 29 July 2020
#===================================================================================
from tkinter import *

garnish = 'garnish.txt'

def writegarnish():
    f = open(garnish, 'w')
    f.write('%s\n' % g1_var.get())
    f.write('%s\n' % g2_var.get())
    f.write('%s\n' % g3_var.get())
    f.close() # close the file

def readgarnish():
    f = open(garnish, "r")  # open this file

    field = 0
    for line in f.readlines():   # read lines
        line = line.rstrip('\n') # remove the carriage return /n at the end of each line
        if field == 0:
            g1_var.set(int(line))
            field += 1
        elif field == 1:
            g2_var.set(int(line))
            field += 1
        elif field == 2:
            g3_var.set(int(line))
    f.close()

root = Tk()
g1_var = IntVar()
g2_var = IntVar()
g3_var = IntVar()

g1 = Checkbutton(root, text="Cheese", variable=g1_var).pack()
g2 = Checkbutton(root, text="Sauce", variable=g2_var).pack()
g3 = Checkbutton(root, text="Pickles", variable=g3_var).pack()

Button(root,text="write to file",command=writegarnish).pack()
Button(root,text="read from file",command=readgarnish).pack()

root.mainloop()
