#==========================================================================
# Display an image on an image using labels on a label
#  Warren Sutton 27 Jul 2020
#==========================================================================
from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk

root = Tk()  # A root window for displaying objects
# two images
imageHead = Image.open('map.jpg')
imageHand = Image.open('person.png')

tkimage = ImageTk.PhotoImage(imageHead)
tkimage2 = ImageTk.PhotoImage(imageHand)

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

# create the label for the map image and span it over multiple rows and columns
panel1 = Label(mainframe, image=tkimage)
panel1.grid(row=0, column=0, rowspan=10, columnspan=10)

# place the person image at a suitable location
person = Label(mainframe, image=tkimage2)
person.grid(row=3, column=5)

person1 = Label(mainframe, image=tkimage2)
person1.grid(row=6, column=5)

# this following line of code removes
person.destroy()

root.mainloop()  # Start the GUI
