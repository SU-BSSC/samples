#===========================================================================
#
# This program demonstrates the use of placing one image on top of another
# image on a tk canvas
#
#===========================================================================
from tkinter import *
root = Tk()

w = 1000
h = 1000
px = 400
py = 100


my_canvas = Canvas(root, width=w, height=h)
my_canvas.pack()

img1 = PhotoImage(file="map.png")
my_image1 = my_canvas.create_image(0, 0, anchor=NW, image=img1)

img = PhotoImage(file="person.png")
my_image = my_canvas.create_image(px, py, anchor=NW, image=img)

# deletes the top image on the canvas
# my_canvas.delete(my_image)

root.mainloop()
